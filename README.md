Vocabulary
==========

## Backend API

### Words (api/v1/words)

* GET ''
* GET ':id'
* POST ''
* PUT ':id'
* DELETE ':id'


### Memory palaces (api/v1/memory-palaces)

* GET ''
* GET ':id'
* POST ''
* PUT ':id'
* DELETE ':id'

## Database models

### Word

* id (uuid)
* name (string)
* translation (string)
* grammar (string)
* story (string)
* created_at (datetime)
* updated_at (datetime)
* memory_palace_id (uuid)
* memory_palace_room_id (uuid)
* user_id (uuid)

### Memory Palace

* id (uuid)
* name (string)
* user_id (uuid)

### Memory Palace Room

* id (uuid)
* name (string)
* index (int)
* memory_palace_id (uuid)

### User

* email (string)
* external_id (string)

### Set up postgres database
docker-compose exec database createdb -U postgres vocabulary
docker-compose exec backend ./manage.py migrate
docker-compose exec backend ./manage.py createsuperuser

### Log in to db
docker-compose exec database psql -h localhost -p 5432 -U postgres vocabulary

