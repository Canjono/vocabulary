"""URL patterns."""

from django.conf.urls import url, include
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from vocabulary.core import views as core_views

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(r"^$", core_views.home, name="home"),
]
