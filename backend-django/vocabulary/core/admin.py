from django.contrib import admin

from .models import MemoryPalace, MemoryPalaceRoom, Word


@admin.register(MemoryPalace)
class MemoryPalaceAdmin(admin.ModelAdmin):

    list_display = ["name", "user"]
    search_fields = ["name"]


@admin.register(MemoryPalaceRoom)
class MemoryPalaceRoomAdmin(admin.ModelAdmin):

    list_display = ["name", "index", "memory_palace"]
    search_fields = ["name"]


@admin.register(Word)
class WordAdmin(admin.ModelAdmin):

    list_display = ["name", "translation", "grammar"]
    search_fields = ["name", "translation"]
