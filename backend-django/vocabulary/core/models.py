"""Memory palace app models."""

import uuid

from django.contrib.auth.models import User
from django.db import models
from model_utils.models import TimeStampedModel


class MemoryPalace(TimeStampedModel):
    """Memory palace model."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    user = models.ForeignKey(
        User, related_name="memory_palaces", on_delete=models.CASCADE
    )

    def __str__(self):
        """Return string representation of MemoryPalace object."""
        return "{} - user {}".format(self.name, self.user)


class MemoryPalaceRoom(TimeStampedModel):
    """Memory palace room model."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    index = models.PositiveIntegerField()
    memory_palace = models.ForeignKey(
        "MemoryPalace", related_name="rooms", on_delete=models.CASCADE
    )

    def __str__(self):
        """Return string representation of MemoryPalaceRoom object."""
        return "{} - memory palace {}".format(self.name, self.memory_palace)


class Word(TimeStampedModel):
    """Word model."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    translation = models.TextField()
    grammar = models.TextField()
    story = models.TextField()
    memory_palace = models.ForeignKey(
        "MemoryPalace", related_name="words", on_delete=models.CASCADE
    )
    memory_palace_room = models.ForeignKey(
        "MemoryPalaceRoom", related_name="words", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User, related_name="words", on_delete=models.CASCADE
    )

    def __str__(self):
        """Return string representation of MemoryPalaceRoom object."""
        return "{} - user {}".format(self.name, self.user)
