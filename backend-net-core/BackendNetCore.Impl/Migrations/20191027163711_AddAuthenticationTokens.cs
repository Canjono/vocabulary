﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendNetCore.Impl.Migrations
{
    public partial class AddAuthenticationTokens : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "authentication_tokens",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    access_token = table.Column<string>(nullable: false),
                    refresh_token = table.Column<string>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false),
                    user_id = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_authentication_tokens", x => x.id);
                    table.ForeignKey(
                        name: "fk_authentication_tokens_asp_net_users_user_id",
                        column: x => x.user_id,
                        principalTable: "asp_net_users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_authentication_tokens_user_id",
                table: "authentication_tokens",
                column: "user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "authentication_tokens");
        }
    }
}
