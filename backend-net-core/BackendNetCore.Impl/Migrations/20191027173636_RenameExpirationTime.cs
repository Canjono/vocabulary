﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendNetCore.Impl.Migrations
{
    public partial class RenameExpirationTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "expiration_time",
                table: "authentication_tokens",
                newName: "refresh_expiration_time");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "refresh_expiration_time",
                table: "authentication_tokens",
                newName: "expiration_time");
        }
    }
}
