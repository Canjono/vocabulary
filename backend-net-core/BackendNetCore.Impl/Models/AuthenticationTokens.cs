using System;
using System.ComponentModel.DataAnnotations;

namespace BackendNetCore.Impl.Models
{
    public class AuthenticationTokens
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string AccessToken { get; set; }

        [Required]
        public string RefreshToken { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime RefreshExpirationTime { get; set; }

        public Guid? UserId { get; set; }

        public User User { get; set; }
    }
}