using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BackendNetCore.Impl.Models
{
    public class MemoryPalace
    {
        [Key]
        public Guid Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        public List<MemoryPalaceRoom> Rooms { get; set; }
        
        public Guid? UserId { get; set; }
        
        public User User { get; set; }
    }
}