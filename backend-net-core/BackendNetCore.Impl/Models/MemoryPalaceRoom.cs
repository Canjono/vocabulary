using System;
using System.ComponentModel.DataAnnotations;

namespace BackendNetCore.Impl.Models
{
    public class MemoryPalaceRoom
    {
        [Key]
        public Guid Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        public int Index { get; set; }
        
        public Guid? MemoryPalaceId { get; set; }
        
        public MemoryPalace MemoryPalace { get; set; }
    }
}