using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace BackendNetCore.Impl.Models
{
    public class User : IdentityUser<Guid>
    {
        public string ExternalId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public List<Word> Words { get; set; }
        public List<MemoryPalace> MemoryPalaces { get; set; }
    }
}