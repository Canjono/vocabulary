using System;
using System.ComponentModel.DataAnnotations;

namespace BackendNetCore.Impl.Models
{
    public class Word
    {
        [Key]
        public Guid Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        public string Translation { get; set; }
        
        public string Grammar { get; set; }
        
        public string Story { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
        
        public MemoryPalace MemoryPalace { get; set; }
        
        public MemoryPalaceRoom MemoryPalaceRoom { get; set; }
        
        public Guid? UserId { get; set; }
        
        public User User { get; set; }
    }
}