using System;
using System.Collections.Generic;
using System.Linq;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories.Interfaces;
using BackendNetCore.Impl.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BackendNetCore.Impl.Repositories.EfCore
{
    public class WordsRepository : IWordsRepository
    {
        private readonly VocabularyContext _context;
        private readonly IUserService _userService;

        public WordsRepository(VocabularyContext context, IUserService userService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        public IEnumerable<Word> GetWords()
        {
            var userId = _userService.GetCurrentUserId();
            var words = _context.Words.Where(x => x.UserId == userId).ToList();

            return words;
        }

        public Word GetWord(Guid id)
        {
            var userId = _userService.GetCurrentUserId();
            var word = _context.Words
                .Include(x => x.MemoryPalace)
                .ThenInclude(x => x.Rooms)
                .Include(x => x.MemoryPalaceRoom)
                .FirstOrDefault(x => x.Id == id && x.UserId == userId);

            return word;
        }

        public Guid? CreateWord(Word word)
        {
            word.UserId = _userService.GetCurrentUserId();
            _context.Words.Add(word);
            _context.SaveChanges();

            return word.Id;
        }

        public bool UpdateWord(Word word)
        {
            var userId = _userService.GetCurrentUserId();
            var wordInDb = _context.Words
                .Include(x => x.MemoryPalace)
                .Include(x => x.MemoryPalaceRoom)
                .FirstOrDefault(x => x.Id == word.Id && x.UserId == userId);

            if (wordInDb == null)
            {
                return false;
            }

            var memoryPalace = word.MemoryPalace == null
                ? null
                : _context.MemoryPalaces.FirstOrDefault(x => x.Id == word.MemoryPalace.Id);
            var room = word.MemoryPalaceRoom == null
                ? null
                : _context.MemoryPalaceRooms.FirstOrDefault(x => x.Id == word.MemoryPalaceRoom.Id);

            wordInDb.Grammar = word.Grammar;
            wordInDb.Name = word.Name;
            wordInDb.Story = word.Story;
            wordInDb.Translation = word.Translation;
            wordInDb.UpdatedAt = word.UpdatedAt;
            wordInDb.MemoryPalace = memoryPalace;
            wordInDb.MemoryPalaceRoom = room;

            _context.SaveChanges();

            return true;
        }

        public bool DeleteWord(Guid id)
        {
            var userId = _userService.GetCurrentUserId();
            var word = _context.Words.FirstOrDefault(x => x.Id == id && x.UserId == userId);

            if (word == null)
            {
                return false;
            }

            _context.Words.Remove(word);
            _context.SaveChanges();

            return true;
        }
    }
}