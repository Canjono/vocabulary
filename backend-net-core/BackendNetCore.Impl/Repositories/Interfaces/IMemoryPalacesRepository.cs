using System;
using System.Collections.Generic;
using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Repositories.Interfaces
{
    public interface IMemoryPalacesRepository
    {
        IEnumerable<MemoryPalace> GetMemoryPalaces();
        MemoryPalace GetMemoryPalace(Guid id);
        Guid? CreateMemoryPalace(MemoryPalace memoryPalace);
        bool UpdateMemoryPalace(MemoryPalace memoryPalace);
        bool DeleteMemoryPalace(Guid id);
        MemoryPalaceRoom GetMemoryPalaceRoom(Guid id);
    }
}