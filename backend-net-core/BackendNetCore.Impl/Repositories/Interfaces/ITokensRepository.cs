using System;
using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Repositories.Interfaces
{
    public interface ITokensRepository
    {
        bool CreateTokens(AuthenticationTokens tokens);
        AuthenticationTokens GetTokens(string accessToken, string refreshToken, Guid? userId);
        bool DeleteTokens(Guid id);
        bool DeleteUserTokens(Guid userId);
    }
}