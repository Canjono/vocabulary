using System;
using System.Collections.Generic;
using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Repositories.Interfaces
{
    public interface IWordsRepository
    {
        IEnumerable<Word> GetWords();
        Word GetWord(Guid id);
        Guid? CreateWord(Word word);
        bool UpdateWord(Word word);
        bool DeleteWord(Guid id);
    }
}