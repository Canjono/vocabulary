using System;
using BackendNetCore.Impl.Extensions;
using BackendNetCore.Impl.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BackendNetCore.Impl.Repositories
{
    public class VocabularyContext : IdentityDbContext<User, Role, Guid>
    {
        public VocabularyContext(DbContextOptions<VocabularyContext> options) : base(options)
        {
        }

        public DbSet<Word> Words { get; set; }
        public DbSet<MemoryPalace> MemoryPalaces { get; set; }
        public DbSet<MemoryPalaceRoom> MemoryPalaceRooms { get; set; }
        public DbSet<AuthenticationTokens> AuthenticationTokens { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            foreach (var entity in builder.Model.GetEntityTypes())
            {
                entity.Relational().TableName = entity.Relational().TableName.ToSnakeCase();

                foreach (var property in entity.GetProperties())
                {
                    property.Relational().ColumnName = property.Relational().ColumnName.ToSnakeCase();
                }

                foreach (var key in entity.GetKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToSnakeCase();
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToSnakeCase();
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.Relational().Name = index.Relational().Name.ToSnakeCase();
                }
            }
        }
    }
}