using System;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories.Interfaces;
using BackendNetCore.Impl.Services.Interfaces;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Identity;

namespace BackendNetCore.Impl.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository _userRepository;
        private readonly UserManager<User> _userManager;
        private readonly ITokenService _tokenService;

        public AuthenticationService(IUserRepository userRepository, UserManager<User> userManager, ITokenService tokenService)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _tokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
        }

        public AuthenticationTokens AuthenticateGoogle(string idToken)
        {
            var payload = _tokenService.ValidateGoogleIdToken(idToken);

            if (payload == null)
            {
                return null;
            }

            var user = GetOrCreateUser(payload);

            if (user == null)
            {
                return null;
            }

            var tokens = _tokenService.CreateTokens(user);

            return tokens;
        }

        public void Logout()
        {
            _tokenService.DeleteUserTokens();
        }

        private User GetOrCreateUser(GoogleJsonWebSignature.Payload payload)
        {
            var user = _userRepository.GetUser(payload.Subject);

            if (user == null)
            {
                user = new User
                {
                    UserName = payload.Email,
                    Email = payload.Email,
                    ExternalId = payload.Subject,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow
                };

                var result = _userManager.CreateAsync(user).Result;

                if (!result.Succeeded)
                {
                    return null;
                }
            }

            return user;
        }
    }
}