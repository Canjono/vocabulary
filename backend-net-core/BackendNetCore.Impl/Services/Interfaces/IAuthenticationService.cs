using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Services.Interfaces
{
    public interface IAuthenticationService
    {
        AuthenticationTokens AuthenticateGoogle(string idToken);
        void Logout();
    }
}