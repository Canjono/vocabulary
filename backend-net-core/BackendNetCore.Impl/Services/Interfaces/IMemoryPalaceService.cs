using System;
using System.Collections.Generic;
using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Services.Interfaces
{
    public interface IMemoryPalaceService
    {
        IEnumerable<MemoryPalace> GetMemoryPalaces();
        MemoryPalace GetMemoryPalace(Guid id);
        Guid? CreateMemoryPalace(MemoryPalace memoryPalace);
        bool UpdateMemoryPalace(MemoryPalace memoryPalace);
        bool DeleteMemoryPalace(Guid id);
    }
}