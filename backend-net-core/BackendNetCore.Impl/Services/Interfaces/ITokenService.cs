using BackendNetCore.Impl.Models;
using Google.Apis.Auth;

namespace BackendNetCore.Impl.Services.Interfaces
{
    public interface ITokenService
    {
        AuthenticationTokens CreateTokens(User user);
        GoogleJsonWebSignature.Payload ValidateGoogleIdToken(string idToken);
        AuthenticationTokens Refresh(AuthenticationTokens tokens);
        bool DeleteUserTokens();
    }
}