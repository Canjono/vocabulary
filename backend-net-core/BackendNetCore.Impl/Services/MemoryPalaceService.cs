using System;
using System.Collections.Generic;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories.Interfaces;
using BackendNetCore.Impl.Services.Interfaces;

namespace BackendNetCore.Impl.Services
{
    public class MemoryPalaceService : IMemoryPalaceService
    {
        private readonly IMemoryPalacesRepository _memoryPalacesRepository;

        public MemoryPalaceService(IMemoryPalacesRepository memoryPalacesRepository)
        {
            _memoryPalacesRepository = memoryPalacesRepository ??
                                       throw new ArgumentNullException(nameof(memoryPalacesRepository));
        }

        public IEnumerable<MemoryPalace> GetMemoryPalaces()
        {
            var memoryPalaces = _memoryPalacesRepository.GetMemoryPalaces();

            return memoryPalaces;
        }

        public MemoryPalace GetMemoryPalace(Guid id)
        {
            var memoryPalace = _memoryPalacesRepository.GetMemoryPalace(id);

            return memoryPalace;
        }

        public Guid? CreateMemoryPalace(MemoryPalace memoryPalace)
        {
            var memoryPalaceId = _memoryPalacesRepository.CreateMemoryPalace(memoryPalace);

            return memoryPalaceId;
        }

        public bool UpdateMemoryPalace(MemoryPalace memoryPalace)
        {
            var success = _memoryPalacesRepository.UpdateMemoryPalace(memoryPalace);

            return success;
        }

        public bool DeleteMemoryPalace(Guid id)
        {
            var success = _memoryPalacesRepository.DeleteMemoryPalace(id);

            return success;
        }
    }
}