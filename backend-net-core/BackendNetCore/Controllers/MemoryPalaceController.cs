using System;
using System.Collections.Generic;
using System.Linq;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Services.Interfaces;
using BackendNetCore.Models.Requests;
using BackendNetCore.Models.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BackendNetCore.Controllers
{
    [Authorize]
    [Route("api/memory-palaces")]
    public class MemoryPalaceController : BaseController
    {
        private readonly IMemoryPalaceService _memoryPalaceService;

        public MemoryPalaceController(IMemoryPalaceService memoryPalaceService)
        {
            _memoryPalaceService =
                memoryPalaceService ?? throw new ArgumentNullException(nameof(memoryPalaceService));
        }

        [HttpGet("")]
        public ActionResult<IEnumerable<GetMemoryPalacesResponse>> GetMemoryPalaces()
        {
            var memoryPalaces = _memoryPalaceService.GetMemoryPalaces();
            var memoryPalacesDto = memoryPalaces.Select(x => new GetMemoryPalacesResponse
            {
                Id = x.Id,
                Name = x.Name,
                Rooms = x.Rooms?.Select(y => new GetMemoryPalaceRoomResponse
                {
                    Id = y.Id,
                    Index = y.Index,
                    Name = y.Name
                }).ToList()
            });

            return Ok(memoryPalacesDto);
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<GetMemoryPalaceResponse>> GetMemoryPalace([FromRoute] Guid? id)
        {
            if (id == null)
            {
                return BadRequest("You must send a valid guid/uuid");
            }

            var memoryPalace = _memoryPalaceService.GetMemoryPalace(id.Value);

            if (memoryPalace == null)
            {
                return NotFound($"Couldn't find memory palace with id: {id}");
            }

            var memoryPalaceDto = new GetMemoryPalaceResponse
            {
                Id = memoryPalace.Id,
                Name = memoryPalace.Name,
                Rooms = memoryPalace.Rooms?.Select(x => new GetMemoryPalaceRoomResponse
                {
                    Id = x.Id,
                    Index = x.Index,
                    Name = x.Name
                }).ToList()
            };

            return Ok(memoryPalaceDto);
        }

        [HttpPost("")]
        public ActionResult<Guid> CreateMemoryPalace([FromBody] CreateMemoryPalaceRequest request)
        {
            var memoryPalace = new MemoryPalace
            {
                Name = request.Name,
                Rooms = new List<MemoryPalaceRoom>()
            };

            for (var i = 0; i < request.Rooms.Count; i++)
            {
                var room = new MemoryPalaceRoom
                {
                    Index = i,
                    Name = request.Rooms[i]
                };

                memoryPalace.Rooms.Add(room);
            }

            var memoryPalaceId = _memoryPalaceService.CreateMemoryPalace(memoryPalace);

            return memoryPalaceId == null
                ? StatusCode(500, "Coulnd't create memory palace")
                : Ok(memoryPalaceId);
        }

        [HttpPut("{id}")]
        public ActionResult UpdateWord([FromRoute] Guid? id, [FromBody] UpdateMemoryPalaceRequest request)
        {
            if (id == null)
            {
                return BadRequest("You must send a valid guid/uuid");
            }

            var memoryPalace = new MemoryPalace
            {
                Id = id.Value,
                Name = request.Name,
                Rooms = request.Rooms?.Select(x => new MemoryPalaceRoom
                {
                    Id = x.Id ?? Guid.Empty,
                    Index = x.Index,
                    Name = x.Name
                }).ToList()
            };

            var success = _memoryPalaceService.UpdateMemoryPalace(memoryPalace);

            if (!success)
            {
                return NotFound();
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteWord([FromRoute] Guid? id)
        {
            if (id == null)
            {
                return BadRequest("You must send a valid guid/uuid");
            }

            var success = _memoryPalaceService.DeleteMemoryPalace(id.Value);

            if (!success)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}