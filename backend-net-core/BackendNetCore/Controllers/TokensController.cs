using System;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Services.Interfaces;
using BackendNetCore.Models.Requests;
using BackendNetCore.Models.Responses;
using Microsoft.AspNetCore.Mvc;

namespace BackendNetCore.Controllers
{
    [Route("api/tokens")]
    public class TokensController : BaseController
    {
        private readonly ITokenService _tokenService;

        public TokensController(ITokenService tokenService)
        {
            _tokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
        }
        
        [HttpPost("refresh")]
        public ActionResult<AuthenticationTokens> Refresh([FromBody] RefreshTokenRequest request)
        {
            var tokens = new AuthenticationTokens
            {
                AccessToken = request.AccessToken,
                RefreshToken = request.RefreshToken
            };

            var refreshedTokens = _tokenService.Refresh(tokens);

            if (refreshedTokens == null)
            {
                return BadRequest("session_expired");
            }
            
            var tokensDto = new RefreshTokensResponse
            {
                AccessToken = refreshedTokens.AccessToken,
                RefreshToken = refreshedTokens.RefreshToken
            };
            
            return Ok(tokensDto);
        }
    }
}