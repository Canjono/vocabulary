using System;

namespace BackendNetCore.Models.Requests
{
    public class CreateWordRequest
    {
        public string Name { get; set; }
        public string Translation { get; set; }
        public string Grammar { get; set; }
        public string Story { get; set; }
        public Guid? MemoryPalaceId { get; set; }
        public Guid? MemoryPalaceRoomId { get; set; }
    }
}