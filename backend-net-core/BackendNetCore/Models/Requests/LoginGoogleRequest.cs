namespace BackendNetCore.Models.Requests
{
    public class LoginGoogleRequest
    {
        public string IdToken { get; set; }
    }
}