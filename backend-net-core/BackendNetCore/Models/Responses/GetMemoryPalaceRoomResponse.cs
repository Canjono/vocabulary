using System;

namespace BackendNetCore.Models.Responses
{
    public class GetMemoryPalaceRoomResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Index { get; set; }
    }
}