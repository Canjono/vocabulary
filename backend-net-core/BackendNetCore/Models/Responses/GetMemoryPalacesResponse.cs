using System;
using System.Collections.Generic;

namespace BackendNetCore.Models.Responses
{
    public class GetMemoryPalacesResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<GetMemoryPalaceRoomResponse> Rooms { get; set; }
    }
}