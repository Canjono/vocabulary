using System;

namespace BackendNetCore.Models.Responses
{
    public class GetWordsResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Translation { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}