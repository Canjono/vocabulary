namespace BackendNetCore.Models.Responses
{
    public class LoginGoogleResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}