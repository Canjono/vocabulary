namespace BackendNetCore.Models.Responses
{
    public class RefreshTokensResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}