﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories;
using BackendNetCore.Impl.Repositories.EfCore;
using BackendNetCore.Impl.Repositories.Interfaces;
using BackendNetCore.Impl.Services;
using BackendNetCore.Impl.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace BackendNetCore
{
    public class Startup
    {
        private readonly bool _useSwagger = Environment.GetEnvironmentVariable("USE_SWAGGER") == "true";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<VocabularyContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(config =>
                {
                    config.RequireHttpsMetadata = false;
                    config.SaveToken = true;
                    config.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Environment.GetEnvironmentVariable("JWT_ISSUER"),
                        ValidAudience = Environment.GetEnvironmentVariable("JWT_AUDIENCE"),
                        ValidateLifetime = true,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(
                                Encoding.UTF8.GetBytes(Environment.GetEnvironmentVariable("JWT_AUTH_SECRET"))),
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddTransient<IMemoryPalaceService, MemoryPalaceService>();
            services.AddTransient<IMemoryPalacesRepository, MemoryPalacesRepository>();
            services.AddTransient<ITokenService, TokenService>();
            services.AddTransient<ITokensRepository, TokensRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IWordService, WordService>();
            services.AddTransient<IWordsRepository, WordsRepository>();

            services.AddCors();

            services
                .AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });

            services.AddEntityFrameworkNpgsql().AddDbContext<VocabularyContext>(builder =>
            {
                builder.UseNpgsql(Environment.GetEnvironmentVariable("CONNECTION_STRING"));
            });

            if (_useSwagger)
            {
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new Info {Title = "BackendDotNet", Version = "1.0"});
                });
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Enable cross-origin requests.
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });

            if (_useSwagger)
            {
                app.UseSwagger(c => c.RouteTemplate = "swagger/{documentName}/swagger.json");
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = "swagger";
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Backend Dot Net");
                });
            }

            app.UseAuthentication();

            app.UseMvc();

            RunDbMigrations(app);
        }

        private void RunDbMigrations(IApplicationBuilder app)
        {
            Console.WriteLine("Running db migrations...");

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var vocabularyContext = serviceScope.ServiceProvider.GetService<VocabularyContext>();

                try
                {
                    vocabularyContext.Database.Migrate();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }
    }
}