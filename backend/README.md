Backend of Vocabulary project
=============================

#### Example queries in GraphiQL (http://localhost:5000/graphql):

##### Get one word

```
{
  getWord(id:"4d8611b1-94ea-4641-9778-0b084d8935ec") {
    id
    name
    translation
    grammar
    story
    createdAt
    updatedAt
  }
}
```

##### Get words

```
{
  getWords {
    id
    name
    translation
    grammar
    story
    createdAt
    updatedAt
    memoryPalaceRoom {
      id
      name
    }
  }
}
```

##### Add word

```
mutation {
  addWord(name: "Korosu", translation: "Kill", grammar: "Verb", story: "He killed him by CRUSHING him", memoryPalaceRoom: "0f9283f9-16c3-48c0-b9f8-15e0aa1df455") {
    id
    name
    translation
    grammar
    story
    createdAt
    updatedAt
    memoryPalaceRoom {
      id
      name
    }
  }
}

```

##### Update word

```
mutation {
  updateWord(id:"321a8c89-5c89-4c37-bc73-3f38653be58a", name:"Korosu", translation:"Kill", grammar:"Verb", story:"He killed him by CRUSHING him"){
    id name translation grammar story createdAt updatedAt
  }
}
```

##### Delete word

```
mutation {
  deleteWord(id:"321a8c89-5c89-4c37-bc73-3f38653be58a")
}
```

##### Get one memory palace

```
{
  getMemoryPalace(id:"c82758ef-4286-42d3-b0e7-1ceb2b086610") {
    id
    name
    memoryPalaceRooms {
      id name createdAt updatedAt
    }
    createdAt
    updatedAt
  }
}
```

##### Get all memory palaces

```
{
  getMemoryPalaces {
    id
    name
    memoryPalaceRooms { id name createdAt updatedAt }
    createdAt
    updatedAt
  }
}
```

##### Add new memory palace

```
mutation {
  addMemoryPalace(name:"Vuollerim",memoryPalaceRooms:["Knabben","Kyrkan","Konsum"]) {
    id
    name
    memoryPalaceRooms { id name createdAt updatedAt }
    createdAt
    updatedAt
  }
}
```

##### Update memory palace

```
mutation {
  updateMemoryPalace(id: "bdf90ead-57b9-45e6-8656-ea5c96376244", name: "Vuollerim2", memoryPalaceRooms:["hi", "there", "duder", "whats", "going", "on"]) {
    id
    name
    memoryPalaceRooms {
      id
      name
      createdAt
      updatedAt
    }
    createdAt
    updatedAt
  }
}
```


##### Delete memory palace

```
mutation {
  deleteMemoryPalace(id:"b1624d66-9d2a-457b-9470-ac2bcc5be84f")
}
```