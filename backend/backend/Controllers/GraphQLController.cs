using System;
using System.Linq;
using System.Threading.Tasks;
using backend.Mutations;
using backend.Queries;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("graphql")]
    public class GraphQLController : Controller
    {
        private readonly VocabularyQuery _query;
        private readonly VocabularyMutation _mutation;

        public GraphQLController(VocabularyQuery query, VocabularyMutation mutation)
        {
            _query = query ?? throw new ArgumentNullException(nameof(query));
            _mutation = mutation ?? throw new ArgumentNullException(nameof(mutation));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            var inputs = query.Variables.ToInputs();

            var schema = new Schema
            {
                Query = _query,
                Mutation = _mutation
            };

            var result = await new DocumentExecuter().ExecuteAsync(_ =>
            {
                _.Schema = schema;
                _.Query = query.Query;
                _.OperationName = query.OperationName;
                _.Inputs = inputs;
            }).ConfigureAwait(false);

            if (result.Errors?.Count > 0)
            {
                Console.WriteLine($"Error: {result.Errors.FirstOrDefault()}");
                return BadRequest($"Error: {result.Errors.FirstOrDefault()}");
            }

            return Ok(result);
        }
    }
}