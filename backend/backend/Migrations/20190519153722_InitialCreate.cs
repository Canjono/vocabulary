﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace backend.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "memory_palaces",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    enabled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_memory_palaces", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "memory_palace_rooms",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: true),
                    updated_at = table.Column<DateTime>(nullable: true),
                    memory_palace_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_memory_palace_rooms", x => x.id);
                    table.ForeignKey(
                        name: "FK_memory_palace_rooms_memory_palaces_memory_palace_id",
                        column: x => x.memory_palace_id,
                        principalTable: "memory_palaces",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "words",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    translation = table.Column<string>(nullable: true),
                    grammar = table.Column<string>(nullable: true),
                    story = table.Column<string>(nullable: true),
                    created_at = table.Column<DateTime>(nullable: false),
                    updated_at = table.Column<DateTime>(nullable: false),
                    enabled = table.Column<bool>(nullable: false),
                    memory_palace_room_id = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_words", x => x.id);
                    table.ForeignKey(
                        name: "FK_words_memory_palace_rooms_memory_palace_room_id",
                        column: x => x.memory_palace_room_id,
                        principalTable: "memory_palace_rooms",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_memory_palace_rooms_memory_palace_id",
                table: "memory_palace_rooms",
                column: "memory_palace_id");

            migrationBuilder.CreateIndex(
                name: "IX_words_memory_palace_room_id",
                table: "words",
                column: "memory_palace_room_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "words");

            migrationBuilder.DropTable(
                name: "memory_palace_rooms");

            migrationBuilder.DropTable(
                name: "memory_palaces");
        }
    }
}
