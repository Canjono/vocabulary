using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models.Database
{
    [Table("memory_palaces")]
    public class MemoryPalace
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }
        
        [Required]
        [Column("name")]
        public string Name { get; set; }
        
        [Column("created_at")]
        public DateTime? CreatedAt { get; set; }
        
        [Column("updated_at")]
        public DateTime? UpdatedAt { get; set; }
        
        [Column("enabled")]
        public bool Enabled { get; set; }
        
        public List<MemoryPalaceRoom> MemoryPalaceRooms { get; set; }
    }
}
