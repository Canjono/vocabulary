using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models.Database
{
    [Table("memory_palace_rooms")]
    public class MemoryPalaceRoom
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }
        
        [Required]
        [Column("name")]
        public string Name { get; set; }
        
        [Column("created_at")]
        public DateTime? CreatedAt { get; set; }
        
        [Column("updated_at")]
        public DateTime? UpdatedAt { get; set; }
        
        [ForeignKey("MemoryPalace")]
        [Column("memory_palace_id")]
        public Guid MemoryPalaceId { get; set; }
        
        public MemoryPalace MemoryPalace { get; set; }
    }
}
