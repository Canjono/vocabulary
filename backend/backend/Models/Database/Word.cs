using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models.Database
{
    [Table("words")]
    public class Word
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }
        
        [Required]
        [Column("name")]
        public string Name { get; set; }
        
        [Column("translation")]
        public string Translation { get; set; }
        
        [Column("grammar")]
        public string Grammar { get; set; }
        
        [Column("story")]
        public string Story { get; set; }
        
        [Required]
        [Column("created_at")]
        public DateTime? CreatedAt { get; set; }
        
        [Required]
        [Column("updated_at")]
        public DateTime? UpdatedAt { get; set; }
        
        [Column("enabled")]
        public bool Enabled { get; set; }
        
        [ForeignKey("MemoryPalaceRoom")]
        [Column("memory_palace_room_id")]
        public Guid? MemoryPalaceRoomId { get; set; }
        public MemoryPalaceRoom MemoryPalaceRoom { get; set; }
    }
}
