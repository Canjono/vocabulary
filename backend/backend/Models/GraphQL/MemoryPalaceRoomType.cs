using backend.Models.Database;
using GraphQL.Types;

namespace backend.Models.GraphQL
{
    public class MemoryPalaceRoomType : ObjectGraphType<MemoryPalaceRoom>
    {
        public MemoryPalaceRoomType()
        {
            Name = "MemoryPalaceRoom";

            Field(x => x.Id, type: typeof(IdGraphType)).Description("The ID of the room.");
            Field(x => x.Name, type: typeof(StringGraphType)).Description("The name of the room");
            Field(x => x.CreatedAt, type: typeof(DateGraphType)).Description("The date the room was created");
            Field(x => x.UpdatedAt, type: typeof(DateGraphType)).Description("The date the room was last updated");
        }
    }
}