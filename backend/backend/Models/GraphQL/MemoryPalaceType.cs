using backend.Models.Database;
using GraphQL.Types;

namespace backend.Models.GraphQL
{
    public class MemoryPalaceType : ObjectGraphType<MemoryPalace>
    {
        public MemoryPalaceType()
        {
            Name = "MemoryPalace";

            Field(x => x.Id, type: typeof(IdGraphType)).Description("The ID of the memory palace.");
            Field(x => x.Name, type: typeof(StringGraphType)).Description("The name of the memory palace");
            Field(x => x.MemoryPalaceRooms, type: typeof(ListGraphType<MemoryPalaceRoomType>)).Description("The rooms of the memory palace");
            Field(x => x.CreatedAt, type: typeof(DateGraphType)).Description("The date the memory palace was created");
            Field(x => x.UpdatedAt, type: typeof(DateGraphType)).Description("The date the memory palace was last updated");
        }
    }
}