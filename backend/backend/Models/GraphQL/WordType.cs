using backend.Models.Database;
using GraphQL.Types;

namespace backend.Models.GraphQL
{
    public class WordType : ObjectGraphType<Word>
    {
        public WordType()
        {
            Name = "Word";

            Field(x => x.Id, type: typeof(IdGraphType)).Description("The ID of the word.");
            Field(x => x.Name, type: typeof(StringGraphType)).Description("The name of the word");
            Field(x => x.Translation, type: typeof(StringGraphType)).Description("The translation of the word");
            Field(x => x.Grammar, type: typeof(StringGraphType)).Description("The grammar of the word");
            Field(x => x.Story, type: typeof(StringGraphType)).Description("The story of the word");
            Field(x => x.CreatedAt, type: typeof(DateGraphType)).Description("The date the word was created");
            Field(x => x.UpdatedAt, type: typeof(DateGraphType)).Description("The date the word was last updated");
            Field(x => x.MemoryPalaceRoom, type: typeof(MemoryPalaceRoomType))
                .Description("The memory palace room");
        }  
    }
}