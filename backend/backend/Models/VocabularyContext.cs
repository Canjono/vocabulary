﻿using backend.Models.Database;
using Microsoft.EntityFrameworkCore;

namespace backend.Models
{
    public class VocabularyContext : DbContext
    {
        public VocabularyContext()
        {
        }

        public VocabularyContext(DbContextOptions<VocabularyContext> options)
            : base(options)
        {
        }

        public DbSet<Word> Words { get; set; }
        public DbSet<MemoryPalace> MemoryPalaces { get; set; }
        public DbSet<MemoryPalaceRoom> MemoryPalaceRooms { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Database=Vocabulary;Username=postgres;Password=postgres");
            }
        }
    }
}
