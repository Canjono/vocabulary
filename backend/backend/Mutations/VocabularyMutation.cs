using System;
using System.Collections.Generic;
using System.Linq;
using backend.Models;
using backend.Models.Database;
using backend.Models.GraphQL;
using backend.Repositories.Interfaces;
using GraphQL.Types;

namespace backend.Mutations
{
    public class VocabularyMutation : ObjectGraphType
    {
        public VocabularyMutation(IWordRepository wordRepository, IMemoryPalaceRepository memoryPalaceRepository)
        {
            if (wordRepository == null) { throw new ArgumentNullException(nameof(wordRepository)); }
            if (memoryPalaceRepository == null) { throw new ArgumentNullException(nameof(memoryPalaceRepository)); }

            Field<WordType>(
                "addWord",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> {Name = "Name", Description = "The name of the word."},
                    new QueryArgument<StringGraphType> {Name = "Translation", Description = "The translation of the word."},
                    new QueryArgument<StringGraphType> {Name = "Grammar", Description = "The grammar of the word."},
                    new QueryArgument<StringGraphType> {Name = "Story", Description = "The story of the word."},
                    new QueryArgument<StringGraphType> {Name = "MemoryPalaceRoom", Description = "Id of the memory palace room"}
                ),
                resolve: context =>
                {
                    var word = new Word
                    {
                        Id = Guid.NewGuid(),
                        Name = context.GetArgument<string>("name"),
                        Translation = context.GetArgument<string>("translation") ?? "",
                        Grammar = context.GetArgument<string>("grammar") ?? "",
                        Story = context.GetArgument<string>("story") ?? "",
                        CreatedAt = DateTime.UtcNow,
                        UpdatedAt = DateTime.UtcNow,
                        MemoryPalaceRoomId = context.GetArgument<Guid?>("memoryPalaceRoom"),
                        Enabled = true
                    };
                    return wordRepository.AddWord(word);
                });

            Field<WordType>(
                "updateWord",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> {Name = "id", Description = "The ID of the word."},
                    new QueryArgument<NonNullGraphType<StringGraphType>> {Name = "Name", Description = "The name of the word."},
                    new QueryArgument<NonNullGraphType<StringGraphType>> {Name = "Translation", Description = "The translation of the word."},
                    new QueryArgument<NonNullGraphType<StringGraphType>> {Name = "Grammar", Description = "The grammar of the word."},
                    new QueryArgument<NonNullGraphType<StringGraphType>> {Name = "Story", Description = "The story of the word."}
                ),
                resolve: context =>
                {
                    var word = new Word
                    {
                        Id = context.GetArgument<Guid>("id"),
                        Name = context.GetArgument<string>("name"),
                        Translation = context.GetArgument<string>("translation"),
                        Grammar = context.GetArgument<string>("grammar"),
                        Story = context.GetArgument<string>("story"),
                        UpdatedAt = DateTime.UtcNow
                    };

                    return wordRepository.UpdateWord(word);
                });
            
            Field<BooleanGraphType>(
                "deleteWord",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "The ID of the word." }),
                resolve: context =>
                {
                    var id = context.GetArgument<Guid>("id");
                    return wordRepository.DeleteWord(id);
                });
            
            Field<MemoryPalaceType>(
                "addMemoryPalace",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> {Name = "Name", Description = "Name of the memory palace."},
                    new QueryArgument<ListGraphType<StringGraphType>> {Name = "MemoryPalaceRooms", Description = "Names of the rooms of the memory palace."}
                ),
                resolve: context =>
                {
                    var roomNames = context.GetArgument<List<string>>("memoryPalaceRooms");

                    var rooms = roomNames.Select(x => new MemoryPalaceRoom
                        {Id = Guid.NewGuid(), Name = x, CreatedAt = DateTime.UtcNow, UpdatedAt = DateTime.UtcNow}).ToList();
                    
                    var memoryPalace = new MemoryPalace
                    {
                        Id = Guid.NewGuid(),
                        Name = context.GetArgument<string>("name"),
                        MemoryPalaceRooms = rooms,
                        CreatedAt = DateTime.UtcNow,
                        UpdatedAt = DateTime.UtcNow,
                        Enabled = true
                    };
                    return memoryPalaceRepository.AddMemoryPalace(memoryPalace);
                });
            
            Field<MemoryPalaceType>(
            "updateMemoryPalace",
            arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<IdGraphType>> {Name = "Id", Description = "The ID of the memory palace"},
                new QueryArgument<NonNullGraphType<StringGraphType>> {Name = "Name", Description = "The name of the memory palace."},
                new QueryArgument<ListGraphType<StringGraphType>> {Name = "MemoryPalaceRooms", Description = "Names of the rooms of the memory palace."}
            ),
            resolve: context =>
            {
                var roomNames = context.GetArgument<List<string>>("memoryPalaceRooms");

                var rooms = roomNames.Select(x => new MemoryPalaceRoom
                    {Id = Guid.NewGuid(), Name = x, CreatedAt = DateTime.UtcNow, UpdatedAt = DateTime.UtcNow}).ToList();
                    
                var memoryPalace = new MemoryPalace
                {
                    Id = context.GetArgument<Guid>("id"),
                    Name = context.GetArgument<string>("name"),
                    MemoryPalaceRooms = rooms,
                    UpdatedAt = DateTime.UtcNow,
                    CreatedAt = DateTime.UtcNow
                };
                return memoryPalaceRepository.UpdateMemoryPalace(memoryPalace);
            });
            
            Field<BooleanGraphType>(
                "deleteMemoryPalace",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "The ID of the memory palace." }),
                resolve: context =>
                {
                    var id = context.GetArgument<Guid>("id");
                    return memoryPalaceRepository.DeleteMemoryPalace(id);
                });
        }
    }
}