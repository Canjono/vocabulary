using System;
using backend.Models.GraphQL;
using backend.Repositories.Interfaces;
using GraphQL;
using GraphQL.Types;

namespace backend.Queries
{
    public class VocabularyQuery : ObjectGraphType
    {
        public VocabularyQuery(IWordRepository wordRepository, IMemoryPalaceRepository memoryPalaceRepository)
        {
            if (wordRepository == null)
            {
                throw new ArgumentNullException(nameof(wordRepository));
            }

            if (memoryPalaceRepository == null)
            {
                throw new ArgumentNullException(nameof(memoryPalaceRepository));
            }

            Field<WordType>(
                "getWord",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> {Name = "id", Description = "The ID of the word."}),
                resolve: context =>
                {
                    var id = context.GetArgument<Guid>("id");
                    var word = wordRepository.GetWord(id);

                    if (word == null)
                    {
                        context.Errors.Add(new ExecutionError($"Couldn't find user with id: {id}"));
                    }

                    return word;
                });

            Field<ListGraphType<WordType>>(
                "getWords",
                resolve: context => wordRepository.GetWords());

            Field<MemoryPalaceType>(
                "getMemoryPalace",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> {Name = "id", Description = "The ID of the memory palace."}),
                resolve: context =>
                {
                    var id = context.GetArgument<Guid>("id");
                    var memoryPalace = memoryPalaceRepository.GetMemoryPalace(id);

                    if (memoryPalace == null)
                    {
                        context.Errors.Add(new ExecutionError($"Couldn't find memory palace with id: {id}"));
                    }

                    return memoryPalace;
                });

            Field<ListGraphType<MemoryPalaceType>>(
                "getMemoryPalaces",
                resolve: context => memoryPalaceRepository.GetMemoryPalaces());
        }
    }
}