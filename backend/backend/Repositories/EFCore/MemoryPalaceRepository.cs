using System;
using System.Collections.Generic;
using System.Linq;
using backend.Models;
using backend.Models.Database;
using backend.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Repositories.EFCore
{
    public class MemoryPalaceRepository : IMemoryPalaceRepository
    {
        private readonly VocabularyContext _context;

        public MemoryPalaceRepository(VocabularyContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public MemoryPalace GetMemoryPalace(Guid id)
        {
            var memoryPalace = _context.MemoryPalaces.Include(x => x.MemoryPalaceRooms)
                .FirstOrDefault(x => x.Id == id);

            return memoryPalace;
        }

        public IEnumerable<MemoryPalace> GetMemoryPalaces()
        {
            var memoryPalaces = _context.MemoryPalaces
                .Where(x => x.Enabled)
                .Include(x => x.MemoryPalaceRooms)
                .ToList();

            return memoryPalaces;
        }

        public MemoryPalace AddMemoryPalace(MemoryPalace memoryPalace)
        {
            _context.MemoryPalaces.Add(memoryPalace);
            _context.SaveChanges();

            return memoryPalace;
        }

        public MemoryPalace UpdateMemoryPalace(MemoryPalace updatedMemoryPalace)
        {
            var dbMemoryPalace = _context.MemoryPalaces.Include(x => x.MemoryPalaceRooms)
                .FirstOrDefault(x => x.Id == updatedMemoryPalace.Id);

            if (dbMemoryPalace == null)
            {
                return null;
            }

            dbMemoryPalace.Name = updatedMemoryPalace.Name;
            dbMemoryPalace.UpdatedAt = updatedMemoryPalace.UpdatedAt;

            var updatedRoomsAmount = updatedMemoryPalace.MemoryPalaceRooms.Count;
            var dbRoomsAmount = dbMemoryPalace.MemoryPalaceRooms.Count;

            if (dbRoomsAmount > updatedRoomsAmount)
            {
                var roomsToRemove = dbMemoryPalace.MemoryPalaceRooms.GetRange(updatedRoomsAmount, dbRoomsAmount);
                _context.RemoveRange(roomsToRemove);
            }

            for (var i = 0; i < updatedMemoryPalace.MemoryPalaceRooms.Count; i++)
            {
                var newRoom = updatedMemoryPalace.MemoryPalaceRooms[i];

                if (i < dbMemoryPalace.MemoryPalaceRooms.Count)
                {
                    var dbRoom = dbMemoryPalace.MemoryPalaceRooms[i];
                    dbRoom.Name = newRoom.Name;
                    dbRoom.UpdatedAt = newRoom.UpdatedAt;
                }
                else
                {
                    dbMemoryPalace.MemoryPalaceRooms.Add(newRoom);
                }
            }

            _context.SaveChanges();

            return updatedMemoryPalace;
        }

        public bool DeleteMemoryPalace(Guid id)
        {
            var memoryPalace = _context.MemoryPalaces.Include(x => x.MemoryPalaceRooms)
                .FirstOrDefault(x => x.Id == id);

            if (memoryPalace == null)
            {
                return false;
            }

            _context.Remove(memoryPalace);
            _context.RemoveRange(memoryPalace.MemoryPalaceRooms);
            _context.SaveChanges();

            return true;
        }
    }
}