using System;
using System.Collections.Generic;
using System.Linq;
using backend.Models;
using backend.Models.Database;
using backend.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Repositories.EFCore
{
    public class WordRepository : IWordRepository
    {
        private readonly VocabularyContext _context;

        public WordRepository(VocabularyContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Word GetWord(Guid id)
        {
            return _context.Words
                .FirstOrDefault(x => x.Enabled && x.Id == id);
        }

        public IEnumerable<Word> GetWords()
        {
            return _context.Words
                .Where(x => x.Enabled)
                .Include(x => x.MemoryPalaceRoom);
        }

        public Word AddWord(Word word)
        {
            _context.Words.Add(word);
            _context.SaveChanges();

            return word;
        }

        public Word UpdateWord(Word word)
        {
            var updateWord = _context.Words.First(x => x.Id == word.Id);
            updateWord.Name = word.Name;
            updateWord.Translation = word.Translation;
            updateWord.Grammar = word.Grammar;
            updateWord.Story = word.Story;
            updateWord.UpdatedAt = word.UpdatedAt;

            _context.SaveChanges();

            return word;
        }

        public bool DeleteWord(Guid id)
        {
            var word = _context.Words.First(x => x.Id == id);
            word.Enabled = false;
            _context.SaveChanges();

            return true;
        }
    }
}