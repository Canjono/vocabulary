using System;
using System.Collections.Generic;
using backend.Models;
using backend.Models.Database;

namespace backend.Repositories.Interfaces
{
    public interface IMemoryPalaceRepository
    {
        MemoryPalace GetMemoryPalace(Guid id);
        IEnumerable<MemoryPalace> GetMemoryPalaces();
        MemoryPalace AddMemoryPalace(MemoryPalace memoryPalace);
        MemoryPalace UpdateMemoryPalace(MemoryPalace memoryPalace);
        bool DeleteMemoryPalace(Guid id);
    }
}