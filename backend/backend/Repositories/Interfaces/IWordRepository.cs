using System;
using System.Collections.Generic;
using backend.Models;
using backend.Models.Database;

namespace backend.Repositories.Interfaces
{
    public interface IWordRepository
    {
        Word GetWord(Guid id);
        IEnumerable<Word> GetWords();
        Word AddWord(Word word);
        Word UpdateWord(Word word);
        bool DeleteWord(Guid id);
    }
}