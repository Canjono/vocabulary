### How to make a backup in pgAdmin 4

* Right click the database and choose Backup
* Enter a filename (restore-vocabulary) and pick Format tar
* In Dump options activate Pre-data, Data, Post-Data, Blobs and Verbose messages
* Click on the Backup button


### How to restore database in pgAdmin 4

* Create a database called Vocabulary manually
* Right click the database and choose Restore
* Format should be 'Custom or tar' and Filename should be the restore-vocabulary.tar file
* In Restore options activate Pre-data, Data, Post-data and Verbose messages
* Click on the Restore button


### Get Backup to use pg_dump version 11

If you can't create a backup because of server version mismatch you can
install PostgreSQL 11 on your computer and use the pgAdmin that comes
with the installation. Make sure that PostgreSQL isn't running afterwards.
On Windows you can disable it in Services.