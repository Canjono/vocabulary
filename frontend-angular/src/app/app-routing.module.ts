import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateMemoryPalaceComponent } from './components/create-memory-palace/create-memory-palace.component';
import { CreateWordComponent } from './components/create-word/create-word.component';
import { LoginComponent } from './components/login/login.component';
import { MemoryPalaceComponent } from './components/memory-palace/memory-palace.component';
import { MemoryPalacesComponent } from './components/memory-palaces/memory-palaces.component';
import { WordComponent } from './components/word/word.component';
import { WordsComponent } from './components/words/words.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'words', component: WordsComponent },
  { path: 'words/:id', component: WordComponent },
  { path: 'create-word', component: CreateWordComponent },
  { path: 'memory-palaces', component: MemoryPalacesComponent },
  { path: 'memory-palaces/:id', component: MemoryPalaceComponent },
  { path: 'create-memory-palace', component: CreateMemoryPalaceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
