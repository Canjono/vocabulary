import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthServiceConfig, SocialLoginModule } from 'angularx-social-login';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateMemoryPalaceComponent } from './components/create-memory-palace/create-memory-palace.component';
import { CreateWordComponent } from './components/create-word/create-word.component';
import { LoginComponent } from './components/login/login.component';
import { MemoryPalaceComponent } from './components/memory-palace/memory-palace.component';
import { MemoryPalacesComponent } from './components/memory-palaces/memory-palaces.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { WordComponent } from './components/word/word.component';
import { WordsComponent } from './components/words/words.component';
import { getAuthServiceConfig } from './configs/auth-service-config';
import { httpInterceptorProviders } from './interceptors';
import { MaterialModule } from './material.module';

@NgModule({
  declarations: [
    AppComponent,
    WordsComponent,
    WordComponent,
    CreateWordComponent,
    ToolbarComponent,
    MemoryPalacesComponent,
    MemoryPalaceComponent,
    CreateMemoryPalaceComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    SocialLoginModule
  ],
  providers: [
    httpInterceptorProviders,
    { provide: AuthServiceConfig, useFactory: getAuthServiceConfig },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
