import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMemoryPalaceComponent } from './create-memory-palace.component';

describe('CreateMemoryPalaceComponent', () => {
  let component: CreateMemoryPalaceComponent;
  let fixture: ComponentFixture<CreateMemoryPalaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMemoryPalaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMemoryPalaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
