import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private errorHandler: ErrorHandlerService
    ) { }

  ngOnInit() { }

  signInWithGoogle(): void {
    this.sessionService.signInWithGoogle().subscribe(
      () => this.router.navigate(['/words']),
      error => this.errorHandler.handleError(error)
    );
  }
}
