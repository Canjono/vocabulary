import { Component, OnInit } from '@angular/core';
import { MemoryPalace } from 'src/app/models/memory-palace';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { MemoryPalaceService } from 'src/app/services/memory-palace.service';

@Component({
  selector: 'app-memory-palaces',
  templateUrl: './memory-palaces.component.html',
  styleUrls: ['./memory-palaces.component.scss']
})
export class MemoryPalacesComponent implements OnInit {
  memoryPalaces: MemoryPalace[] = [];

  constructor(
    private memoryPalaceService: MemoryPalaceService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.getMemoryPalaces();
  }

  private getMemoryPalaces(): void {
    this.memoryPalaceService.getMemoryPalaces().subscribe(
      memoryPalaces => this.memoryPalaces = memoryPalaces.sort(this.sortMemoryPalaces),
      error => this.errorHandler.handleError(error)
    );
  }

  private sortMemoryPalaces(palace1: MemoryPalace, palace2: MemoryPalace): number {
      if (palace1.name > palace2.name) {
        return 1;
      }

      if (palace1.name < palace2.name) {
        return -1;
      }

      return 0;
  }

}
