import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {
  isLoggedIn = false;
  isLoggedIn$: Subscription;

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private errorHandler: ErrorHandlerService
    ) { }

  ngOnInit() {
    this.sessionService.isLoggedIn().subscribe(
      isLoggedIn => this.setLoggedIn(isLoggedIn),
      error => this.errorHandler.handleError(error)
    );

    this.sessionService.onLoggedInChange.subscribe(
      isLoggedIn => this.setLoggedIn(isLoggedIn),
      error => this.errorHandler.handleError(error)
    );
  }

  logout(): void {
    this.sessionService.signOut().subscribe(
      () => {
        this.setLoggedIn(false);
        this.router.navigate(['/login']);
      },
      error => this.errorHandler.handleError(error)
    );
  }

  setLoggedIn(isLoggedIn: boolean): void {
    this.isLoggedIn = isLoggedIn;

    if (!isLoggedIn) {
      this.router.navigate(['/login']);
    }
  }

  ngOnDestroy(): void {
    this.isLoggedIn$.unsubscribe();
  }
}
