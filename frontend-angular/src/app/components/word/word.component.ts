import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { MemoryPalace } from 'src/app/models/memory-palace';
import { MemoryPalaceRoom } from 'src/app/models/memory-palace-room';
import { Word } from 'src/app/models/word';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { MemoryPalaceService } from 'src/app/services/memory-palace.service';
import { WordService } from 'src/app/services/word.service';

@Component({
  selector: 'app-word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.scss']
})
export class WordComponent implements OnInit {
  memoryPalaces: MemoryPalace[] = [];
  wordId: string;
  form = new FormGroup({
    name: new FormControl(),
    translation: new FormControl(),
    grammar: new FormControl(),
    story: new FormControl(),
    memoryPalace: new FormControl(),
    memoryPalaceRoom: new FormControl()
  });

  constructor(
    private memoryPalaceService: MemoryPalaceService,
    private wordService: WordService,
    private route: ActivatedRoute,
    private location: Location,
    private snackBar: MatSnackBar,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.getWord();
    this.getMemoryPalaces();
  }

  getWord(): void {
    this.wordId = this.route.snapshot.paramMap.get('id');

    this.wordService.getWord(this.wordId).subscribe(
      word => {
        const controls = this.form.controls;
        controls.name.setValue(word.name);
        controls.translation.setValue(word.translation);
        controls.grammar.setValue(word.grammar);
        controls.story.setValue(word.story);
        controls.memoryPalace.setValue(word.memoryPalace);
        controls.memoryPalaceRoom.setValue(word.memoryPalaceRoom);
      },
      error => this.errorHandler.handleError(error)
    );
  }

  getMemoryPalaces(): void {
    this.memoryPalaceService.getMemoryPalaces().subscribe(
      memoryPalaces => {
        this.memoryPalaces = memoryPalaces;
      },
      error => this.errorHandler.handleError(error)
    );
  }

  compareMemoryPalaces(palace1: MemoryPalace, palace2: MemoryPalace): boolean {
    if (!palace1 || !palace2) {
      return false;
    }

    return palace1.id === palace2.id;
  }

  compareRooms(room1: MemoryPalaceRoom, room2: MemoryPalaceRoom): boolean {
    if (!room1 || !room2) {
      return false;
    }

    return room1.id === room2.id;
  }

  update(): void {
    const controls = this.form.controls;
    const word = new Word();
    word.id = this.wordId;
    word.name = controls.name.value;
    word.translation = controls.translation.value;
    word.grammar = controls.grammar.value;
    word.story = controls.story.value;
    word.memoryPalace = controls.memoryPalace.value;
    word.memoryPalaceRoom = controls.memoryPalaceRoom.value;

    this.wordService.updateWord(word).subscribe(
      () => {
        this.goBack();
        this.snackBar.open(`Updated word ${word.name}`, 'Ok', {
          duration: 2000
        });
      },
      error => this.errorHandler.handleError(error)
    );
  }

  delete(): void {
    this.wordService.deleteWord(this.wordId).subscribe(
      () => {
        this.goBack();
        this.snackBar.open(`Deleted word ${this.form.controls.name.value}`, 'Ok', {
          duration: 2000
        });
      },
      error => this.errorHandler.handleError(error)
    );
  }

  goBack(): void {
    this.location.back();
  }
}
