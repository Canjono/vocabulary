import { Component, OnInit } from '@angular/core';
import { Word } from 'src/app/models/word';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { WordService } from 'src/app/services/word.service';

@Component({
  selector: 'app-words',
  templateUrl: './words.component.html',
  styleUrls: ['./words.component.scss']
})
export class WordsComponent implements OnInit {
  words: Word[] = [];

  constructor(
    private wordService: WordService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.getWords();
  }

  private getWords(): void {
    this.wordService.getWords().subscribe(
      words => this.words = words.sort(this.sortWords),
      error => this.errorHandler.handleError(error)
    );
  }

  private sortWords(word1: Word, word2: Word): number {
    if (word1.name > word2.name) {
      return 1;
    }

    if (word1.name < word2.name) {
      return -1;
    }

    return 0;
  }
}
