export class RefreshTokenRequest {
  accessToken: string;
  refreshToken: string;
}
