export class WordCreate {
  name: string;
  translation: string;
  grammar: string;
  story: string;
  memoryPalaceId: string;
  memoryPalaceRoomId: string;
}
