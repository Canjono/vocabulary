import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(private snackbar: MatSnackBar) { }

  handleError(error: HttpErrorResponse): void {
    let errorMessage = 'An error occured';

    if (error.error) {
      errorMessage += ': ' + error.error;
    } else if (error.statusText) {
      errorMessage += ': ' + error.statusText;
    }

    this.snackbar.open(errorMessage, 'Ok', {
      duration: 10000
    });
  }

}
