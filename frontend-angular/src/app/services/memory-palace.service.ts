import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { MemoryPalace } from '../models/memory-palace';
import { MemoryPalaceCreate } from '../models/memory-palace-create';
import { MemoryPalaceUpdate } from '../models/memory-palace-update';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class MemoryPalaceService {
  constructor(
    private httpService: HttpService
  ) { }

  getMemoryPalaces(): Observable<MemoryPalace[]> {
    return this.httpService.get('memory-palaces');
  }

  getMemoryPalace(id: string): Observable<MemoryPalace> {
    return this.httpService.get(`memory-palaces/${id}`);
  }

  createMemoryPalace(memoryPalace: MemoryPalace): Observable<any> {
    const payload = new MemoryPalaceCreate();
    payload.name = memoryPalace.name;
    payload.rooms = memoryPalace.rooms.map(x => x.name);
    return this.httpService.post('memory-palaces', payload);
  }

  updateMemoryPalace(memoryPalace: MemoryPalace): Observable<any> {
    const payload = new MemoryPalaceUpdate();
    payload.name = memoryPalace.name;
    payload.rooms = memoryPalace.rooms;
    return this.httpService.put(`memory-palaces/${memoryPalace.id}`, payload);
  }

  deleteMemoryPalace(id: string): Observable<any> {
    return this.httpService.delete(`memory-palaces/${id}`);
  }
}
