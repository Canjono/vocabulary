import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  sessionTokenKey = 'sessionToken';
  refreshTokenKey = 'refreshToken';

  constructor() { }

  getSessionToken(): string {
    return localStorage.getItem(this.sessionTokenKey);
  }

  setSessionToken(sessionToken: string): void {
    localStorage.setItem(this.sessionTokenKey, sessionToken);
  }

  removeSessionToken(): void {
    localStorage.removeItem(this.sessionTokenKey);
  }

  getRefreshToken(): string {
    return localStorage.getItem(this.refreshTokenKey);
  }

  setRefreshToken(refreshToken: string): void {
    localStorage.setItem(this.refreshTokenKey, refreshToken);
  }

  removeRefreshToken(): void {
    localStorage.removeItem(this.refreshTokenKey);
  }

}
