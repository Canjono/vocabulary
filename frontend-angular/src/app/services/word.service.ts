import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Word } from '../models/word';
import { WordCreate } from '../models/word-create';
import { WordUpdate } from '../models/word-update';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class WordService {
  constructor(private httpService: HttpService) { }

  getWords(): Observable<Word[]> {
    return this.httpService.get('words');
  }

  getWord(id: string): Observable<Word> {
    return this.httpService.get(`words/${id}`);
  }

  createWord(word: Word): Observable<any> {
    const payload = new WordCreate();
    payload.name = word.name;
    payload.translation = word.translation;
    payload.grammar = word.grammar;
    payload.story = word.story;
    payload.memoryPalaceId = word.memoryPalace === null
      ? null
      : word.memoryPalace.id;
    payload.memoryPalaceRoomId = word.memoryPalaceRoom === null
      ? null
      : word.memoryPalaceRoom.id;

    return this.httpService.post('words', payload);
  }

  updateWord(word: Word): Observable<any> {
    const payload = new WordUpdate();
    payload.name = word.name;
    payload.translation = word.translation;
    payload.grammar = word.grammar;
    payload.story = word.story;
    payload.memoryPalaceId = word.memoryPalace === null
      ? null
      : word.memoryPalace.id;
    payload.memoryPalaceRoomId = word.memoryPalaceRoom === null
      ? null
      : word.memoryPalaceRoom.id;

    return this.httpService.put(`words/${word.id}`, payload);
  }

  deleteWord(id: string): Observable<any> {
    return this.httpService.delete(`words/${id}`);
  }
}
